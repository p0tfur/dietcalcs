'use strict';

const menu = document.querySelector(".menu");
const menuPos = menu.getBoundingClientRect();

let menuActive = false;

window.addEventListener("scroll", function()
{
    if(window.pageYOffset >= menuPos.top && !menuActive)
    {
      menu.classList.add("stick");
      menuActive = true;
    }
    else if (window.pageYOffset < menuPos.top && menuActive)
    {
      menu.classList.remove("stick");
      menuActive = false;
    }
});

function calcBMI()
{

  let height = parseFloat(document.getElementById("height").value / 100);
  let weight = parseInt(document.getElementById("weight").value);
  let resultDesc, result;

  console.log(height, typeof height);
  console.log(weight, typeof weight);

  if(height > 0 && weight > 0 && typeof height === "number" && typeof weight === "number")
  {
    result = (weight / (height * height)).toFixed(2);
  }
  else
  {
    alert("Please enter numeric value");
  }

  if(result < 16) { resultDesc = "wygłodzenie"; }
  else if(result >= 16 && result < 17) { resultDesc = "wychudzenie"; }
  else if(result >= 17 && result < 18.50) { resultDesc = "niedowaga"; }
  else if(result >= 18.50 && result < 25) { resultDesc = "wartość prawidłowa"; }
  else if (result >= 25 && result < 30) { resultDesc = "nadwaga"; }
  else if(result >= 30 && result < 35) { resultDesc = "I stopień otyłości"; }
  else if(result >= 35 && result < 40) { resultDesc = "II stopień otyłości"; }
  else if(result >= 40) { resultDesc = "otyłość skrajna"; }

  document.getElementById("resultBMI").innerHTML = `${result} <br /> ${resultDesc}`;
  return false;

};

function calcBF()
{
  let ageBF = parseInt(document.getElementById("bf_age").value);
  let weightBF = parseInt(document.getElementById("bf_weight").value); //use it to count lean body mass!
  let bicepsBF = parseInt(document.getElementById("bf_biceps").value);
  let tricepsBF = parseInt(document.getElementById("bf_triceps").value);
  let subscapularBF = parseInt(document.getElementById("bf_subscapular").value);
  let iliaccrestBF = parseInt(document.getElementById("bf_iliaccrest").value);
  let resultDesc, result;

  result = Math.log10(bicepsBF + tricepsBF + subscapularBF + iliaccrestBF);

  if(document.getElementsByName("bf_gender")[0].checked) // man
  {
      //genderBF = document.getElementsByName("bf_gender")[0].value;

      if(ageBF < 17){ result = 1.1533 - (0.0643 * result); }
      else if(ageBF >= 17 && ageBF <= 19) { result = 1.1620 - (0.0630 * result); }
      else if(ageBF >= 20 && ageBF <= 29) { result = 1.1631 - (0.0632 * result); }
      else if(ageBF >= 30 && ageBF <= 39) { result = 1.1422 - (0.0544 * result); }
      else if(ageBF >= 40 && ageBF <= 49) { result = 1.1620 - (0.0700 * result); }
      else if(ageBF >= 50) { result = 1.1715 - (0.0779 * result); }
  }
  else if(document.getElementsByName("bf_gender")[1].checked) // women
  {
      //genderBF = document.getElementsByName("bf_gender")[1].value;

      if(ageBF < 17) { result = 1.1369 - (0.0598 * result); }
      else if(ageBF >= 17 && ageBF <= 19) { result = 1.1549 - (0.0678 * result); }
      else if(ageBF >= 20 && ageBF <= 29) { result = 1.1599 - (0.0717 * result); }
      else if(ageBF >= 30 && ageBF <= 39) { result = 1.1423 - (0.0632 * result); }
      else if(ageBF >= 40 && ageBF <= 49) { result = 1.1333 - (0.0612 * result); }
      else if(ageBF >= 50) { result = 1.1339 - (0.0645 * result); }
  }
  else
  {
    alert("Please select gender");
  }

  result = ((495 / result) - 450).toFixed(2);

  if(ageBF < 30)
  {
    if(result < 9) { resultDesc = "Very low"; }
    else if(result >= 9 && result < 13) { resultDesc = "Low"; }
    else if(result >= 13 && result < 17) { resultDesc = "Average"; }
    else if(result >= 17 && result < 20) { resultDesc = "Very high"; }
    else if(result > 20) { resultDesc = "Over-fat"; }
  }
  else if(ageBF >= 30 && ageBF < 40)
  {
    if(result < 11) { resultDesc = "Very low"; }
    else if(result >= 11 && result < 14) { resultDesc = "Low"; }
    else if(result >= 14 && result < 18) { resultDesc = "Average"; }
    else if(result >= 18 && result < 23) { resultDesc = "Very high"; }
    else if(result > 23) { resultDesc = "Over-fat"; }
  }
  else if(ageBF>= 40 && ageBF< 50)
  {
    if(result < 12) { resultDesc = "Very low"; }
    else if(result >= 12 && result < 16) { resultDesc = "Low"; }
    else if(result >= 16 && result < 21) { resultDesc = "Average"; }
    else if(result >= 21 && result < 26) { resultDesc = "Very high"; }
    else if(result > 26) { resultDesc = "Over-fat"; }
  }
  else if(ageBF>= 50)
  {
    if(result < 13) { resultDesc = "Very low"; }
    else if(result >= 13 && result < 17) { resultDesc = "Low"; }
    else if(result >= 17 && result < 22) { resultDesc = "Average"; }
    else if(result >= 22 && result < 28) { resultDesc = "Very high"; }
    else if(result > 28) { resultDesc = "Over-fat"; }
  }

  let lbm = (weightBF - (weightBF * result / 100)).toFixed(2);

  document.getElementById("resultBF").innerHTML = `${result} % - ${resultDesc} <br />Lean body mass: ${lbm} kg`;
  return false;

};

function calcBMR()
{
  let ageBMR = parseInt(document.getElementById("bmr_age").value);
  let heightBMR = parseInt(document.getElementById("bmr_height").value);
  let weightBMR = parseInt(document.getElementById("bmr_weight").value);
  let result;

  if(document.getElementsByName("bmr_gender")[0].checked) // man
  {
      result = 66 + (13.7 * weightBMR) + (5 * heightBMR) - (6.8 * ageBMR);
  }
  else if(document.getElementsByName("bmr_gender")[1].checked) // women
  {
      result = 655 + (9.6 * weightBMR) + (1.8 * heightBMR) - (4.7 * ageBMR);
  }
  else
  {
    alert("Please select gender");
  }

  result = Math.round(result * 1e2) / 1e2;

  document.getElementById("resultBMR").innerHTML = `${result} kcal`;
  return false;
};

function calcIW()
{
  let heightIW = parseInt(document.getElementById("iw_height").value);
  let result;

  if(document.getElementsByName("iw_gender")[0].checked) // man
  {
    result = (heightIW - 100) - ((heightIW - 100) * 0.1);
  }
  else if(document.getElementsByName("iw_gender")[1].checked) // women
  {
    result = (heightIW - 100) - ((heightIW - 100) * 0.15);
  }
  else
  {
    alert("Please select gender");
  }

  document.getElementById("resultIW").innerHTML = `${result} kg`;
  return false;
};

function calcWTHR()
{
  let waistWTHR = parseInt(document.getElementById("wthr_waist").value);
  let hipWTHR = parseInt(document.getElementById("wthr_hip").value);
  let result, resultDesc;

  if(document.getElementsByName("wthr_gender")[0].checked) // man
  {
    result = Math.round((waistWTHR / hipWTHR) * 1e2) / 1e2;

    if(result >= 1)
    {
      resultDesc = "High Health Risk";
    }
    else if(result < 1 && result >= 0.96)
    {
      resultDesc = "Moderate Health Risk";
    }
    else if(result <= 0.95)
    {
      resultDesc = "Low Health Risk";
    }
  }
  else if(document.getElementsByName("wthr_gender")[1].checked) // women
  {
    result = Math.round((waistWTHR / hipWTHR) * 1e2) / 1e2;

    if(result >= 0.85)
    {
      resultDesc = "High Health Risk";
    }
    else if(result < 0.85 && result >= 0.81)
    {
      resultDesc = "Moderate Health Risk";
    }
    else if(result <= 0.80)
    {
      resultDesc = "Low Health Risk";
    }
  }
  else
  {
    alert("Please select gender");
  }

  document.getElementById("resultWTHR").innerHTML = `${result} <br> ${resultDesc}`;
  return false;
}

window.addEventListener("hashchange", function () 
{
  window.scrollTo(window.scrollX, window.scrollY - 200);
});